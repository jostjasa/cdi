﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CDI.Models;

namespace CDI.Controllers
{
    public class MaestroDetalleController : Controller
    {
        EntitiesCDI contexto = null;
        // GET: MaestroDetalle
        public ActionResult Index()
        {
            List<CAJEROM> cajerom = new List<CAJEROM>();
            using (contexto = new EntitiesCDI())
            {
                //cajerom = contexto.CAJEROM.Take(5).ToList();
                cajerom = contexto.CAJEROM.ToList();
            }
            return View(cajerom);
            
        }
        
        public class ATMDetail
        {
            public int A_Id { get; set; }
            public int A_ObjectKey { get; set; }
            public string A_EmailAut { get; set; }
            public string A_NombreContacto { get; set; }
            public int A_TelfContacto { get; set; }
            public string A_ObsrvContacto { get; set; }
            public string A_Observaciones { get; set; }
            public string A_HASlmDispensador { get; set; }
            public string A_HASlmOtrosModulos { get; set; }
            public string A_HAFlmTDV { get; set; }
            public string A_HAFlmTOC { get; set; }
            public string A_HSAtm { get; set; }
            public string A_HSOficina { get; set; }
            public string A_ATMUptimeU6 { get; set; }
            public string A_TransaccU6 { get; set; }
        }

        public JsonResult GetATMDetailsByOrderId(int objectId)
        {
            try
            {
                using (contexto = new EntitiesCDI())
                {

                    //var blogs = contexto.ATM
                    //    .Where(ad => ad.A_ObjectKey==objectId)
                    //    .ToList();

                    IQueryable<ATM> blogs = contexto.ATM.Where(ad => ad.A_ObjectKey == objectId);
                    List<ATMDetail> atmDetails = new List<ATMDetail>();
                    foreach (var ad in blogs)
                    {
                        ATMDetail ATMDetail = new ATMDetail();
                        ATMDetail.A_ObjectKey = ad.A_ObjectKey;
                        ATMDetail.A_EmailAut = ad.A_EmailAut;
                        ATMDetail.A_NombreContacto = ad.A_NombreContacto;
                        ATMDetail.A_TelfContacto = ad.A_TelfContacto;
                        ATMDetail.A_ObsrvContacto = ad.A_ObsrvContacto;
                        ATMDetail.A_Observaciones = ad.A_Observaciones;
                        ATMDetail.A_HASlmDispensador = ad.A_HASlmDispensador;
                        ATMDetail.A_HASlmOtrosModulos = ad.A_HASlmOtrosModulos;
                        ATMDetail.A_HAFlmTDV = ad.A_HAFlmTDV;
                        ATMDetail.A_HAFlmTOC = ad.A_HAFlmTOC;
                        ATMDetail.A_HSAtm = ad.A_HSAtm;
                        ATMDetail.A_HSOficina = ad.A_HSOficina;
                        ATMDetail.A_ATMUptimeU6 = ad.A_ATMUptimeU6;
                        ATMDetail.A_TransaccU6 = ad.A_TransaccU6;
                        atmDetails.Add(ATMDetail);
                    }

                    return Json(new { Results = atmDetails }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }






        [HttpPost]
        public ActionResult Add(ATMDetail model)
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                return View();
            }
        }
    }
}