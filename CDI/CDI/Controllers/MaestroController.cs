﻿using CDI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using CDI.Models.ViewModels;
using CDI.ViewModels;

namespace CDI.Controllers
{
    public class MaestroController : Controller
    {
        ApplicationDbContext context;

        public MaestroController()
        {
            context = new ApplicationDbContext();
        }

        // GET: Maestro


        //[HttpPost]
        public ActionResult Index(int? objectkey, int pagina = 1)
        {
            var cantidadRegistrosPorPagina = 5; // parámetro
            using (var db = new EntitiesCDI())
            {
                Func<CajeroMaestro, bool> predicado = x => !objectkey.HasValue || objectkey.Value == x.C_ObjectKey;

                var cajeros = (from d in db.CAJEROM
                              select new CajeroMaestro
                              {
                                  C_IdCajero = d.C_IdCajero,
                                  C_NombreCajero = d.C_NombreCajero,
                                  C_ObjectKey = d.C_ObjectKey
                              })
                    .OrderBy(x => x.C_ObjectKey)
                    .Skip((pagina - 1) * cantidadRegistrosPorPagina)
                    .Take(cantidadRegistrosPorPagina).ToList();
                var totalDeRegistros = db.CAJEROM.Count();

                var mod = new ViewModels.IndexViewModel();
                mod.Cajeros = cajeros;
                mod.PaginaActual = pagina;
                mod.TotalDeRegistros = totalDeRegistros;
                mod.RegistrosPorPagina = cantidadRegistrosPorPagina;
                mod.ValoresQueryString = new RouteValueDictionary();
                mod.ValoresQueryString["objectkey"] = objectkey;

                return View(mod);
            }
        }




        ///---------------------------------------------------------------------------
        public ActionResult Create()
        {
            var Detalle = new ATMViewModel();
            return View(Detalle);
        }

        [HttpPost]
        public ActionResult Create(ATMViewModel model, int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (EntitiesCDI db = new EntitiesCDI())
                    {
                        var purchCount = db.ATM.OrderByDescending(u => u.A_Id).FirstOrDefault();

                        var oATM = new ATM();
                        oATM.A_Id = purchCount.A_Id+1;
                        oATM.A_ObjectKey = id;
                        oATM.A_EmailAut = model.A_EmailAut;
                        oATM.A_NombreContacto = model.A_NombreContacto;
                        oATM.A_TelfContacto = model.A_TelfContacto;
                        oATM.A_ObsrvContacto = model.A_ObsrvContacto;
                        oATM.A_Observaciones = model.A_Observaciones;
                        oATM.A_HASlmDispensador = model.A_HASlmDispensador;
                        oATM.A_HASlmOtrosModulos = model.A_HASlmOtrosModulos;
                        oATM.A_HAFlmTDV = model.A_HAFlmTDV;
                        oATM.A_HAFlmTOC = model.A_HAFlmTOC;
                        oATM.A_HSAtm = model.A_HSAtm;
                        oATM.A_HSOficina = model.A_HSOficina;
                        oATM.A_ATMUptimeU6 = model.A_ATMUptimeU6;
                        oATM.A_TransaccU6 = model.A_TransaccU6;

                        db.ATM.Add(oATM);
                        db.SaveChanges();
                    }

                    return Redirect("~/Maestro/Index");
                }

                return View(model);


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        ///---------------------------------------------------------------------------

        
        //[HttpPost]
        //public ActionResult Editar(ListUsersViewModel model)
        //{
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            using (EntitiesCDI db = new EntitiesCDI())
        //            {
        //                var oUser = db.AspNetUsers.Find(model.Id);
        //                oUser.Id = model.Id;
        //                oUser.UserName = model.UserName;
        //                oUser.Email = model.Email;

        //                db.Entry(oUser).State = System.Data.Entity.EntityState.Modified;
        //                db.SaveChanges();
        //            }

        //            return Redirect("/AspNetUser/Users");
        //        }

        //        return View(model);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}



    }

}