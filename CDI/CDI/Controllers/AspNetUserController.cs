﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CDI.Models;
using CDI.Models.ViewModels;
using System.Threading.Tasks;
using SweetAlert;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;

namespace CDI.Controllers
{
    public class AspNetUserController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        ApplicationDbContext context;
        public AspNetUserController()
        {
            context = new ApplicationDbContext();
        }

        public AspNetUserController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public Boolean isAdminUser()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var s = UserManager.GetRoles(user.GetUserId());
                if (s[0].ToString() == "Admin")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }




        // GET: AspNetUser
        public ActionResult Users()
        {


            if (User.Identity.IsAuthenticated)
            {


                if (!isAdminUser())
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }





            List<ListUsersViewModel> lst;

            using (EntitiesCDI db = new EntitiesCDI())
            {
                lst = (from d in db.AspNetUsers
                           select new ListUsersViewModel
                           {
                               Id = d.Id,
                               UserName = d.UserName,
                               Email = d.Email
                           }).ToList();
            }
            return View(lst);
        }

        public ActionResult Editar(string Id)
        {
            ListUsersViewModel model = new ListUsersViewModel();
            using (EntitiesCDI db = new EntitiesCDI())
            {
                var oUser = db.AspNetUsers.Find(Id);
                model.UserName = oUser.UserName;
                model.Email = oUser.Email;
                model.Id = oUser.Id;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Editar(ListUsersViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (EntitiesCDI db = new EntitiesCDI())
                    {
                        var oUser = db.AspNetUsers.Find(model.Id);
                        oUser.Id = model.Id;
                        oUser.UserName = model.UserName;
                        oUser.Email = model.Email;

                        db.Entry(oUser).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    
                    return Redirect("/AspNetUser/Users");
                }

                return View(model);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [HttpGet]
        public ActionResult Eliminar(string Id)
        {
            using(EntitiesCDI db = new EntitiesCDI())
            {
                var oUser = db.AspNetUsers.Find(Id);
                db.AspNetUsers.Remove(oUser);
                db.SaveChanges();
            }
            return Redirect("/AspNetUser/Users");
        }


        //// POST: /Manage/ChangePassword
        //[HttpPost]
        //public async Task<ActionResult> Edit(ListUsersViewModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return View(model);
        //    }
        //    var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
        //    if (result.Succeeded)
        //    {
        //        var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
        //        if (user != null)
        //        {
        //            await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
        //        }
        //        return RedirectToAction("Index", new { Message = ManageMessageId.ChangePasswordSuccess });
        //    }
        //    AddErrors(result);
        //    return View(model);
        //}





    }
}