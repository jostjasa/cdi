//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CDI.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ATM
    {
        public int A_Id { get; set; }
        public int A_ObjectKey { get; set; }
        public string A_EmailAut { get; set; }
        public string A_NombreContacto { get; set; }
        public int A_TelfContacto { get; set; }
        public string A_ObsrvContacto { get; set; }
        public string A_Observaciones { get; set; }
        public string A_HASlmDispensador { get; set; }
        public string A_HASlmOtrosModulos { get; set; }
        public string A_HAFlmTDV { get; set; }
        public string A_HAFlmTOC { get; set; }
        public string A_HSAtm { get; set; }
        public string A_HSOficina { get; set; }
        public string A_ATMUptimeU6 { get; set; }
        public string A_TransaccU6 { get; set; }
    
        public virtual CAJEROM CAJEROM { get; set; }
    }
}
