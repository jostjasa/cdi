﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CDI.Models.ViewModels
{
    public class CajeroM
    {
        public List<ATMDetailVM> details { get; set; }
    }

    public class ATMDetailVM
    {        
        public string A_EmailAut { get; set; }
        public string A_NombreContacto { get; set; }
        public int A_TelfContacto { get; set; }
        public string A_ObsrvContacto { get; set; }
        public string A_Observaciones { get; set; }
        public string A_HASlmDispensador { get; set; }
        public string A_HASlmOtrosModulos { get; set; }
        public string A_HAFlmTDV { get; set; }
        public string A_HAFlmTOC { get; set; }
        public string A_HSAtm { get; set; }
        public string A_HSOficina { get; set; }
        public string A_ATMUptimeU6 { get; set; }
        public string A_TransaccU6 { get; set; }
    }
}