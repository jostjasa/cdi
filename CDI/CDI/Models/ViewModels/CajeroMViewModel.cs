﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CDI.Models.ViewModels
{
    public class CajeroMViewModel
    {        
        public int C_ObjectKey { get; set; }
        public string C_IdCajero { get; set; }
        public string C_NombreCajero { get; set; }
        public string C_Direccion { get; set; }
        public string C_Ciudad { get; set; }
        public string C_Tipo { get; set; }
        public string C_TipoCategoria { get; set; }
        public string C_Entidad { get; set; }
        public string C_Administracion { get; set; }
        public string C_Regional { get; set; }
        public string C_Zona { get; set; }
        public string C_Transportadora { get; set; }
        public string C_Sector { get; set; }
        public string C_District { get; set; }
        public string C_Comunicacion { get; set; }

        public List<ATMDetalle> ATMDetalles { get; set; }
    }


    public class ATMDetalle
    {
        public int A_Id { get; set; }
        public int A_ObjectKey { get; set; }
        public string A_EmailAut { get; set; }
        public string A_NombreContacto { get; set; }
        public int A_TelfContacto { get; set; }
        public string A_ObsrvContacto { get; set; }
        public string A_Observaciones { get; set; }
        public string A_HASlmDispensador { get; set; }
        public string A_HASlmOtrosModulos { get; set; }
        public string A_HAFlmTDV { get; set; }
        public string A_HAFlmTOC { get; set; }
        public string A_HSAtm { get; set; }
        public string A_HSOficina { get; set; }
        public string A_ATMUptimeU6 { get; set; }
        public string A_TransaccU6 { get; set; }
    }
}