﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CDI.Models.ViewModels
{
    public class ListUsersViewModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }

    }
}