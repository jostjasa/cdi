﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CDI.Models
{
    public class CajeroMaestro
    {
        public int C_ObjectKey { get; set; }
        public string C_IdCajero { get; set; }
        public string C_NombreCajero { get; set; }
    }
}