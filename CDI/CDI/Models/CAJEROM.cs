//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CDI.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class CAJEROM
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        private CAJEROM()
        {
            this.ATM = new HashSet<ATM>();
        }
    
        public int C_Id { get; set; }
        public int C_ObjectKey { get; set; }
        public string C_IdCajero { get; set; }
        public string C_NombreCajero { get; set; }
        public string C_Direccion { get; set; }
        public string C_Ciudad { get; set; }
        public string C_Tipo { get; set; }
        public string C_TipoCategoria { get; set; }
        public string C_Entidad { get; set; }
        public string C_Administracion { get; set; }
        public string C_Regional { get; set; }
        public string C_Zona { get; set; }
        public string C_Transportadora { get; set; }
        public string C_Sector { get; set; }
        public string C_District { get; set; }
        public string C_Comunicacion { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ATM> ATM { get; set; }
    }
}
